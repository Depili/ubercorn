package main

import (
	"gitlab.com/Depili/ubercorn/unicorn"
	"image/color"
	"time"
)

func main() {

	u, err := unicorn.Init()
	if err != nil {
		panic(err)
	}

	c := color.RGBA{R: 255}
	u.SetPixel(0, 0, c)
	c.R = 0
	c.G = 255
	u.SetPixel(15, 0, c)
	c.G = 0
	c.B = 255
	u.SetPixel(0, 15, c)
	c.R = 255
	c.G = 255
	u.SetPixel(15, 15, c)
	u.Update()

	time.Sleep(2 * time.Second)

	u.Fill(color.RGBA{R: 64, G: 0, B: 0, A: 0})
	u.Update()
	time.Sleep(2 * time.Second)

	u.Close()
}
